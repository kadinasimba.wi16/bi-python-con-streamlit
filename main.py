import pandas as pd
import streamlit as st
import plotly.express as px

# Leer el archivo Excel
dataset = pd.read_excel("TerrorismoSocialAlert.xlsx")

# Configuración de la página en Streamlit
st.set_page_config(page_title="SocialAlert", layout="wide")

# Barra lateral para filtrar por categoría (red por que tiene las redes que vamos a trabajar)
st.sidebar.header("Filter By:")
category = st.sidebar.multiselect("Filter By categoria",
                                  options=dataset["red"].unique(),
                                  default=dataset["red"].unique())

# Filtrar el conjunto de datos según la categoría seleccionada
select_query = dataset[dataset["red"].isin(category)]

# Mostrar título
st.title("Social Alert TERRORISMO")

# Contar el número de filas donde la columna "name" no es nula
total_usuarios = select_query["name"].count()

# Contar el número de registros con Sentimento igual a -1 y 1 , POSITIVO Y NEGATIVO
count_sentiment_minus_1 = (select_query["sentiment"] == -1).sum()
count_sentiment_1 = (select_query["sentiment"] == 1).sum()

#Mostrar métricas en dos columnas
first_column, second_column = st.columns(2)

with first_column:
    st.markdown("### Total de usuarios:")
    st.subheader(f'{total_usuarios}')

with second_column:
    st.markdown("### Número de registros con Sentimento igual a -1(Negativo):")
    st.subheader(f'{count_sentiment_minus_1}')

    st.markdown("### Número de registros con Sentiment igual a 1(positivo):")
    st.subheader(f'{count_sentiment_1}')


st.markdown("----")

# Análisis y visualización por categoría (red)
profit_by_category = select_query.groupby(["red", "pais"]).size().reset_index(name="count")

# Gráfico de barras mejorado
profit_by_category_barchart = px.bar(profit_by_category,
                                     x="red",
                                     y="count",
                                     color="pais",
                                     title="Distribución Geográfica por Red",
                                     labels={"count": "Número de Entradas", "red": "Red"})

# Actualizar el diseño del gráfico de barras
profit_by_category_barchart.update_layout(plot_bgcolor="rgba(0,0,0,0)",
                                           xaxis=dict(showgrid=False),
                                           legend_title_text='País')

# Mostrar el gráfico de barras mejorado
st.plotly_chart(profit_by_category_barchart)

st.markdown("----")

# Gráfico de pastel para la distribución geográfica (país)
geographical_distribution = select_query.groupby(["pais"]).size().reset_index(name="count")

geographical_distribution_piechart = px.pie(geographical_distribution,
                                            names="pais",
                                            values="count",
                                            title="Distribución Geográfica",
                                            hole=0.3)

# Mostrar el gráfico de pastel
st.plotly_chart(geographical_distribution_piechart)
